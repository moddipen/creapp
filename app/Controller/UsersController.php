<?php
App::uses('AppController', 'Controller');
session_start();
App::import('Vendor', 'LinkedIn', array('file' => 'LinkedIn/index.php'));
class UsersController extends AppController
{
	public $helpers = array('Html', 'Form','Session');
	var $components=array("Email","Session","RequestHandler","Image");
	public $uses = array('User');

	var $name = 'Users';

	public function beforeFilter() 
	{
		parent::beforeFilter();
		$this->Auth->allow('browse','signup','forgetpassword','process','confirm','login','about');		
	}

	/**
		This function is for signup
	*/

    public function signup() 
	{	
		$this->set('title','signup');
		//$this->layout=null;
		
		if(!empty($this->request->data))
		{
				
			$token = $this->generateToken(12);
			$this->request->data['token'] = $token;

			$this->User->create();
			//debug($this->request->data);exit;
		
			if($this->User->save($this->request->data))
			{
							    	
			
			$check = $this->User->find('first',array('conditions'=>array('User.id'=>$this->User->getLastInsertID())));	

			if(!empty($check))
			{

				$Email = new CakeEmail();
				$Email->config('smtp');
				$Email->delivery = 'Smtp';
				$Email->from(array(Configure::read('FROMEMAIL')=> Configure::read('SUPPORTEMAIL')));
				$Email->to($this->request->data['User']['email']);
				$Email->subject('Confirmation Message');
				$message = Configure::read('SITE_URL')."users/confirm/".$token;
		    	if ($Email->send($message)){

		    		
		    		$this->Session->setFlash(__('Please confirm your email !...', true), 'default', array('class' => 'alert alert-success'));
					$this->redirect(array('action' => 'login'));
			       	return true;
		    	}
		    }
				$this->Session->setFlash(__('SignUp successfully', true), 'default', array('class' => 'alert alert-success'));
			}
			else
			{
				$this->Session->setFlash(__('Something went wrong', true), 'default', array('class' => 'alert alert-error'));
			}
		}
	}

	/**
		This Function Is For Admin Login.
	*/
	public function login()
	{

		$this->set('title','login');
		//$this->layout=null;
		if($this->request->is('post'))
		{ 
			if($this->Auth->login()) 
			{
				
				$user = $this->User->find("first",array("conditions"=>array("User.id"=>$this->Auth->user('id'),"User.usertype"=>0)));
				if(!empty($user))
				{
					$this->Session->setFlash(__('Login successfully', true), 'default', array('class' => 'alert alert-success'));
					$this->redirect(array('controller'=>'users','action'=>'browse'));
				}	
				else
				{
					$this->Session->setFlash(__('You are not authorized to access this panel', true), 'default', array('class' => 'alert alert-danger'));
				}		
			}
			else
			{
					$this->Session->setFlash(__('Invalid email or password !!....', true), 'default', array('class' => 'alert alert-danger'));
			}
		}		
	}

	/**
		This Function Is For Home Page After Login
	*/
	public function index()
	{
	   
	}

	/**
		This Function Is confirmation after sign up
	*/

	public function confirm($token)
	{
		
		$this->autoRender = false;
		$user = $this->User->find("first",array("conditions"=>array("User.token"=>$token)));
		if(!empty($user)){

			$update = array(
				'id'=>$user['User']['id'],
				'status'=>1,
				'token'=>$this->generateToken(12));

			if($this->User->save($update)){
				$this->Session->setFlash(__('Your account is activated', true), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('action' => 'login'));	
			}
		}else{
			$this->Session->setFlash(__('Invalid Token', true), 'default', array('class' => 'alert alert-danger'));
				$this->redirect(array('action' => 'login'));		
		}
	   
	}		

	/**
      This function is for Admin Profile
    */
	public function profile() 
	{	
		$user_data = $this->User->find("first",array("conditions"=>array("User.id"=>$this->Session->read("Auth.User.id"))));

		$this->set("user_data",$user_data);
	}

	/**
	*  This Function is used to update profile 	  
	*/

    public function editprofile() 
	{	
	
	    $this->set('title','editprofile');
		$this->set('sub_action','editprofile');
		
		$user = $this->User->find('first',array('conditions'=>array('User.usertype' => 0,'User.id'=>$this->Session->read("Auth.User.id"))));

		if($this->request->is('post'))
		{
			//echo "<pre>";
			//print_r($this->request->data);exit();

			
			if(!empty($this->request->data))
			{			 
	          if(!empty($this->request->data['User']['img']['name']))
		        {	        
		           $file2 = $this->request->data['User']['img'];	
		           //debug($file2); exit;			
					$image_path = $this->Image->upload_image_and_thumbnail($this->request->data,'img',Configure::read('big'),Configure::read('medium'),Configure::read('small'),"uploads",false);		           	
					//debug($$image_path); exit;
					$name2 = time().'~'.$file2['name'];				
					$this->request->data['User']['profile'] = $image_path;         		 
	        	}
	        	else
	        	{
	        		unset($this->request->data['User']['profile']);        		
	        	}
			    $this->request->data['User']['usertype'] = 0;
		            
				
				if($this->User->save($this->request->data))
				{											
					$this->Session->setFlash(__('Profile update successfully', true), 'default', array('class' => 'alert alert-success'));
					$this->redirect(array('action' => 'profile'));						
				}
				else
				{
					$this->Session->setFlash(__('something went wrong !!....', true), 'default',array('class' => 'alert alert-danger'));
				}		
			}

		
	   	}

	   	$this->set('user_data',$user);
	}

	/**
		Admin forget password
	*/

	public function forgetpassword()
	{
		$this->set('title','forget password');
		//$this->layout=null;
		if($this->request->is('post'))
		{
			$password = $this->generateRandomPassword(8);	
		

			$email=$this->request->data['User']['email'];
			if(!$email)
			{
				$this->Session->setFlash(__('Please enter email address !...', true), 'default', array('class' => 'alert alert-error'));
				$this->redirect(array('action' => 'forgetpassword','admin'=>true));
			}

			$check=$this->User->find('first',array('conditions' => array('User.email'=>$this->request->data['User']['email'])));

			if(!empty($check))
			{

				$Email = new CakeEmail();
				$Email->config('smtp');
				$Email->delivery = 'Smtp';
				$Email->from(array(Configure::read('FROMEMAIL')=> Configure::read('SUPPORTEMAIL')));
				$Email->to($this->request->data['User']['email']);
				$Email->subject('Forget Password');
				$message = "Your new password is ". $password;
		    	if ($Email->send($message))
		     	{
		    		//$array = array('id'=>$check['User']['id'],'password'=>$password);
				$this->User->id = $check['User']['id'];
		    		if($this->User->id)
		    		{
		    			$this->User->saveField('password',$password);
		    			$this->Session->setFlash(__('New Password has been send to your email !...', true), 'default', array('class' => 'alert alert-success'));
						$this->redirect(array('action' => 'login'));
			       		return true;	
		    		}
		    		else
		    		{
		    			$this->Session->setFlash(__('Unable to update record', true), 'default', array('class' => 'alert alert-error'));
						$this->redirect(array('action' => 'forgetpassword'));
		    		}    	   	
		     	}

		     	else
		     	{
		     		return false;
		     	}
		    }
		    else
		    {
				$this->Session->setFlash(__('This Is not your Email address!...', true), 'default', array('class' => 'alert alert-error'));
				$this->redirect(array('action' => 'forgetpassword'));
		    }	
		}
	}		
    
    /**
		This Function is for linkedIn login
	*/

	public function process()
	{
		$this->layout=null;

		$baseURL = Configure::read('LINKEDIN_BASE_URL');
		$callbackURL = Configure::read('LINKEDIN_CALLBACK_URL');
		$linkedinApiKey = Configure::read('LINKEDIN_API_KEY');
		$linkedinApiSecret = Configure::read('LINKEDIN_API_SECRET');
		$linkedinScope = Configure::read('LINKEDIN_SCOPE');

	  	if (isset($_GET["oauth_problem"]) && $_GET["oauth_problem"] <> "") 
	  	{
		  $_SESSION["err_msg"] = $_GET["oauth_problem"];
		  exit;
		}

		$client = new oauth_client_class;

		$client->debug = false;
		$client->debug_http = true;
		$client->redirect_uri = $callbackURL;

		$client->client_id = $linkedinApiKey;
		$application_line = __LINE__;
		$client->client_secret = $linkedinApiSecret;

		if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0)
		  die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , '.
					'create an application, and in the line '.$application_line.
					' set the client_id to Consumer key and client_secret with Consumer secret. '.
					'The Callback URL must be '.$client->redirect_uri).' Make sure you enable the '.
					'necessary permissions to execute the API calls your application needs.';


		$client->scope = $linkedinScope;
		if (($success = $client->Initialize())) {
		  if (($success = $client->Process())) {
		    if (strlen($client->authorization_error)) {
		      $client->error = $client->authorization_error;
		      $success = false;
		    } elseif (strlen($client->access_token)) {
		      $success = $client->CallAPI(
							'http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 
							'GET', array(
								'format'=>'json'
							), array('FailOnAccessError'=>true), $user);
		    }
		  }

		$success = $client->Finalize($success);
		}
		if ($client->exit) exit;
		if ($success) {
		  	
			$_SESSION['user'] = $user;
			
			$linked_user =  json_decode(json_encode($user), true);

			$new_array = array("email"=>$linked_user['emailAddress'],
				"first_name"=>$linked_user['firstName'],
				"last_name"=>$linked_user['lastName'],
				"profile"=>$linked_user['pictureUrl'],
				"log_linked"=>1
				);
			$this->User->create();
			
			if ($this->User->save($new_array)) {
				
				$get_last_user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->User->getLastInsertID())));
				
				$this->Session->write('Auth', $get_last_user);

			} else 
			{
				
				$get_last_user = $this->User->find('first',array('conditions'=>array('User.email'=>$linked_user['emailAddress'])));
				if (!empty($get_last_user)) {
					
				$this->Session->write('Auth', $get_last_user);	

				} else {
					$this->Session->setFlash(__('No record found', true), 'default', array('class' => 'alert alert-error'));
			 		$this->redirect(array('action' => 'login'));		
				}
				
			}
		
			$this->Session->setFlash(__('Login Successfully', true), 'default', array('class' => 'alert alert-success'));
			$this->redirect(array('action' => 'browse'));
			
		} else {
		 	 $_SESSION["err_msg"] = $client->error;
		 	 $this->Session->setFlash(__('Something went wrong', true), 'default', array('class' => 'alert alert-error'));
			 $this->redirect(array('action' => 'login'));
		}
			   
	}	


	/**
		This Function Is For Logout
	*/

	public function logout() 
	{	

		$user = $this->Session->read('Auth.User');
           // debug($user); exit;    
        if($user['log_linked'] == 1) {  
			session_destroy();
		} else {
			$this->Auth->logout();
		}
		$this->Session->setFlash(__('Successfully Loggedout!!....', true), 'default', array('class' => 'alert alert-success'));
    	 $this->redirect(array('action' => 'browse'));
	}

	public function browse() 
	{
		
	}

	public function about() 
	{ 
		
	}

	public function publish() 
	{	
		
	}

	public function delete(){
		$this->autoRender = false;
		//debug($this->Session->read('Auth.User')); exit;
		$id = $this->Session->read('Auth.User.id'); 
	
			if($this->User->delete($id))
			{
				session_destroy();
				$this->Auth->logout();
				//$this->Session->setFlash(__(' Delete successfully...', true), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('action' => 'browse'));	
			}
			else
			{
				$this->Session->setFlash(__(' unable to delete...', true), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('action' => 'browse'));
			}	

	}
} 
?>