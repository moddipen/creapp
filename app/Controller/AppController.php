<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array('Session','RequestHandler','Cookie','Auth' => array(
        'authenticate' => array(
            'Form' => array(
                'fields' => array('username' => 'email')
            )
        )
    ));
	public function beforeFilter() 
	{
        $this->Auth->authenticate = array( 'Form' => array( 'userModel' => 'User', 'fields' => array( 'username' => 'email', 'password' => 'password')));
  		$this->Auth->autoRedirect = false;
  		Security::setHash("md5");      
    }


/**
        Generate random password
    */
    public function generateRandomPassword($length = 8, $randomString = null) 
    {
        if (empty($randomString)) 
        {
            $randomString = 'ApVqBoOwiNeuQrMytRlaPkCsWjd#ShfIgmUzMnAxbcKv1029384756';
        }
        $newPassword = '';

        for ($i = 0; $i < $length; $i++) 
        {
            $newPassword .= substr($randomString, mt_rand(0, strlen($randomString) - 1), 1);
        }

        return $newPassword;
    }

/**
Generate Token
*/
    function generateToken($length = 12, $randomString = null){
        if (empty($randomString)) 
        {
            $randomString = 'ApVqBoOwiNeuQrMytRlaPkCsWjd#ShfIgmUzMnAxbcKv1029384756';
        }
        $token = '';

        for ($i = 0; $i < $length; $i++) 
        {
            $token .= substr($randomString, mt_rand(0, strlen($randomString) - 1), 1);
        }

        return $token;
}

}
?>