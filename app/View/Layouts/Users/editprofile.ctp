
  
  <!-- Left side column. contains the logo and sidebar -->
  

  <!-- Content Wrapper. Contains page content -->
  
    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <?php echo $this->Form->create('User',array('novalidate' => true,'class'=>'form-horizontal','id'=>'editprofile','enctype'=>'multipart/form-data'));?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update profile</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!--  <?php $detail = $Profile;?>-->  
               <?php 
            $user = $this->Session->read('Auth.User');
           // debug($user); exit;    
            if($user['User']['log_linked'] == 1){    
            ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">First name </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.first_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter first name','value'=>$user['User']['first_name']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Last name </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.last_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter last name','value'=>$user['User']['last_name']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">License </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.license',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Enter license','value'=>$user['User']['license']));
                    ?>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Email </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.email',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter Email','value'=>$user['User']['email']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Telephone </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.phone',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Enter phone number','value'=>$user['User']['phone']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Title </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.title',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter title','value'=>$user['User']['title']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Company Name </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.company_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter company name','value'=>$user['User']['company_name'],'value'=>$user['User']['company_name']));
                    ?>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Picture </label>

                  <div class="col-sm-9">
                    <?php 
                     echo $this->Form->file("User.img",array('doc_file',"label"=>false,'class'=>'pro-img','type' => 'file','id'=>'imgInp'));
                            
                     echo $this->Form->input("User.profile",array("type"=>"hidden","value"=>""));
                    ?>
                  </div>
                  <img src="<?php echo $user['User']['profile'];?>" id="img"  alt="Profile Image" style="width:80px;height:80px;margin-left: -90px;"/> 
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Description </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.description',array('label'=>false,'class'=>'form-control','placeholder'=>'','type'=>'textarea'));
                            
                     
                    ?>
                  </div>
                </div>
                
                
                
                
                    <?php
                      echo $this->Form->input('User.usertype',array('type'=>'hidden','value'=>$user['User']['usertype']));
                      echo $this->Form->input('User.id',array('type'=>'hidden','value'=>$user['User']['id']));
                    ?>
                 
                
                
                
              </div>

              <?php } else { ?>

               <div class="box-body">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">First name </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.first_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter first name','value'=>$user['User']['first_name']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Last name </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.last_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter last name','value'=>$user['User']['last_name']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">License </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.license',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Enter license','value'=>$user['User']['license']));
                    ?>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Email </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.email',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter Email','value'=>$user['User']['email']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Telephone </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.phone',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Enter phone number','value'=>$user['User']['phone']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Title </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.title',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter title','value'=>$user['User']['title']));
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Company Name </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.company_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter company name','value'=>$user['User']['company_name'],'value'=>$user['User']['company_name']));
                    ?>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Picture </label>

                  <div class="col-sm-9">
                    <?php 
                     echo $this->Form->file("User.img",array('doc_file',"label"=>false,'class'=>'pro-img','type' => 'file','id'=>'imgInp'));
                            
                     echo $this->Form->input("User.profile",array("type"=>"hidden","value"=>""));
                    ?>
                  </div>
                  <img src="<?php echo $this->webroot;?>img/uploads/small/<?php echo $user['User']['profile'];?>" id="img"  alt="Profile Image" style="width:80px;height:80px;margin-left: -90px;"/> 
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Description </label>

                  <div class="col-sm-9">
                    <?php 
                    echo $this->Form->input('User.description',array('label'=>false,'class'=>'form-control','placeholder'=>'','type'=>'textarea'));
                            
                     
                    ?>
                  </div>
                </div>
                
                
                
                
                    <?php
                      echo $this->Form->input('User.usertype',array('type'=>'hidden','value'=>$user['User']['usertype']));
                      echo $this->Form->input('User.id',array('type'=>'hidden','value'=>$user['User']['id']));
                    ?>
                 
                
                
                
              </div>


              <?php } ?>
              <!-- /.box-body -->
              <div class="box-footer">
                  <div class="box-footer" align="center">  
                  <button type="submit" id="formsubmit" class="btn btn-primary" style="width:10%;">SAVE CHANGES</button>
                   <!--  </div>   -->           
                    <?php echo $this->Form->end();?>
              </div>

                     <a href="<?php echo Configure::read('SITE_URL')?>users/profile" class="btn btn-primary btn-block"><b>VIEW PROFILE</b></a>

                     <a href="<?php echo Configure::read('SITE_URL')?>users/profile" class="btn btn-primary btn-block"><b>CANCEL</b></a>
                  
              <!-- /.box-footer -->
         
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    
    <!-- /.content -->
  
  <!-- /.content-wrapper -->
 
<style type="text/css">
.btn-block{
  width: 200px;
}
.error-message
{
  color:red;
}
textarea {
   resize: none;
}
.pro-img{
margin-top: 7px;
}
</style>
<?php echo $this->Html->script('validate/jquery.validate.js')?>
<script type="text/javascript">
   
//    $("#editprofile").validate(
//     {
//       rules: {
//             'data[User][first_name]': {required:true},
//             'data[User][last_name]': {required:true},
//             'data[User][email]': {required: true,email: true},
//             'data[User][mobile]': {required: true},
//             'data[User][password]': {required: true},
//             'data[User][confirm_password]': {required: true,equalTo :'#UserPassword'}   
//           },
//           messages: {
//             'data[User][first_name]': "Please enter first name.",
//             'data[User][last_name]': "Please enter last name.",
//             'data[User][email]': "Please enter a valid email.",
//             'data[User][mobile]': "Please enter phone number.",
//             'data[User][password]': "Please enter password.",
//             'data[User][confirm_password]': "Please enter valid confirm password."
//           }
//         });


// $("#editprofile").submit(function(e)
//   {
//     var isvalidate=$("#editprofile").valid();
//     if(!isvalidate)
//     {
//         e.preventDefault();
//     }
//     else
//     {
       
       
//      }   
//   });
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();            
            reader.onload = function (e) {
                $('#img').attr('src', e.target.result);
            }            
            reader.readAsDataURL(input.files[0]);
        }
    }    
    $("#imgInp").change(function(){
        readURL(this);
    });
</script>



