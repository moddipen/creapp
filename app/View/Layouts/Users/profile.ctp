 <?php
    echo $this->Html->css('bootstrap.min.css');
    echo $this->Html->css('AdminLTE.min.css');
    echo $this->Html->css('blue.css');


 //echo $this->Html->script('jQuery-2.2.0.min.js');
 echo $this->Html->script('bootstrap.min.js');
 echo $this->Html->script('icheck.min.js');
 
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Profile</li>

      </ol>
       <a href="<?php echo Configure::read('SITE_URL')?>users/editprofile" class="btn btn-primary btn-block"><b>EDIT PROFILE</b></a>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

            <?php 
            $user = $this->Session->read('Auth.User');
           // debug($user); exit;    
            if($user['User']['log_linked'] == 1){    
            ?>

             <center><img src="<?php echo $user['User']['profile'];?>" class="img-circle" alt="User Image" style="width:150px;height:150px;"></center>


              <h3 class="profile-username text-center"><?php echo $user['User']['first_name'].' '.$user['User']['last_name'];?></h3>
 
            <?php }else{ ?>
             <center><img src="<?php echo $this->webroot; ?>img/uploads/small/<?php echo $user['User']['profile'];?>" class="img-circle" alt="User Image" style="width:150px;height:150px;"></center>

              <h3 class="profile-username text-center"><?php echo $user['User']['first_name'].' '.$user['User']['last_name'];?></h3>
            <?php } ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
 <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.pack.min.js"></script>
<script type="text/javascript">
    $(function($){
        var addToAll = false;
        var gallery = false;
        var titlePosition = 'inside';
        $(addToAll ? 'img' : 'img.fancybox').each(function(){
            var $this = $(this);
            var title = $this.attr('title');
            var src = $this.attr('data-big') || $this.attr('src');
            var a = $('<a href="#" class="fancybox"></a>').attr('href', src).attr('title', title);
            $this.wrap(a);
        });
        if (gallery)
            $('a.fancybox').attr('rel', 'fancyboxgallery');
        $('a.fancybox').fancybox({
            titlePosition: titlePosition
        });
    });
    $.noConflict();
</script>
<link rel="stylesheet" type="text/css" media="screen" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.css" />
<style type="text/css">
.btn-block{
  width: 200px;
}
    a.fancybox img {
        border: none;
        box-shadow: 0 1px 7px rgba(0,0,0,0.6);
        -o-transform: scale(1,1); -ms-transform: scale(1,1); -moz-transform: scale(1,1); -webkit-transform: scale(1,1); transform: scale(1,1); -o-transition: all 0.2s ease-in-out; -ms-transition: all 0.2s ease-in-out; -moz-transition: all 0.2s ease-in-out; -webkit-transition: all 0.2s ease-in-out; transition: all 0.2s ease-in-out;
    } 
    a.fancybox:hover img {
        position: relative; z-index: 999; -o-transform: scale(1.03,1.03); -ms-transform: scale(1.03,1.03); -moz-transform: scale(1.03,1.03); -webkit-transform: scale(1.03,1.03); transform: scale(1.03,1.03);
    }
</style>