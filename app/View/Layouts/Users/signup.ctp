<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Creappus : SignUp</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
 
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
 
  <!-- iCheck -->
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition register-page">
<?php
    echo $this->Html->css('bootstrap.min.css');
    echo $this->Html->css('AdminLTE.min.css');
    echo $this->Html->css('blue.css');


 //echo $this->Html->script('jQuery-2.2.0.min.js');
 echo $this->Html->script('bootstrap.min.js');
  echo $this->Html->script('jquery.min.js');
   
// echo $this->Html->script('icheck.min.js');
 echo $this->Session->flash();
?>
<script>
/*  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });*/
</script>
 
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>CREAPPUS</b></a>
    <center> <h5><?php echo $this->Session->flash();?></h5></center>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <?php echo $this->Form->create('User',array('acion'=>'signup',"id"=>"edit-profile",'novalidate' => true));?>
      <div class="form-group has-feedback">
        <?php 
                   echo $this->Form->input('User.first_name',array('type'=>'text','label'=>false,'div'=>false,'class'=>"form-control",'placeholder'=>'First Name*'));
                   ?>
                  
      </div>
      <div class="form-group has-feedback">
        <?php 
                   echo $this->Form->input('User.last_name',array('type'=>'text','label'=>false,'div'=>false,'class'=>"form-control",'placeholder'=>'Last Name*'));
                   ?>
                  
      </div>
      <div class="form-group has-feedback">
        <?php 
                   echo $this->Form->input('User.email',array('type'=>'text','label'=>false,'div'=>false,'class'=>"form-control",'placeholder'=>' Email*'));
                   ?>
                   
      </div>
      <div class="form-group has-feedback">
        <?php 
                   echo $this->Form->input('User.password',array('label'=>false,'div'=>false,'class'=>"form-control",'placeholder'=>' Password '));
                   ?>
                   
      </div>
      <div class="form-group has-feedback">
        <?php 
                   echo $this->Form->input('User.license',array('type'=>'text','label'=>false,'div'=>false,'class'=>"form-control",'placeholder'=>'RE license#*'));
                   ?>
                  
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I have read the <a href="#">terms and conditions</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">REGISTER</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      
     <!--  <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a> -->
    </div>

    <a href="<?php echo configure::read('SITE_URL');?>users/login" class="text-center">BACK TO LOGIN</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<script type="text/javascript">
 $(document).ready(function() 
    {
      $("#flashMessage").fadeOut(4000, function()
         {
          $("#flashMessage").hide();
        });
    });        
</script>

</body>
</html>
