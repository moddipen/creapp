<title>Restaurant : Forgetpassword</title>
<?php
    echo $this->Html->meta('icon');?>


<?php 
  echo $this->Html->css('bootstrap.min.css');
  echo $this->Html->css('AdminLTE.min.css');
  echo $this->Html->css('font-awesome.min.css');
  echo $this->Html->css('ionicons.min.css');
?>



<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <b>Forget</b> Password
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">
      <center><div id="hide_div">
           <?php echo $this->Session->flash();?>
         </div></center>
        <p class="login-box-msg"><b>Enter Email To Get Your Password</b></p>

        <?php echo $this->Form->create('User',array('action'=>'forgetpassword',"id"=>"edit-profile",'novalidate' => true));?>
          <div class="form-group has-feedback">
            <?php 
              echo $this->Form->input('User.email',array('label'=>false,'div'=>false,'class'=>"form-control",'placeholder'=>' Email'));
            ?>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>

         

          <div class="row">  
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><b>Reset</b></button>
            </div>
            <?php echo $this->Form->end();?>
          </div>
          
    </div>
  </div>
</body>
<script type="text/javascript">
$('#hide_div').show();
$('#hide_div').delay(3000).fadeOut();

</script>