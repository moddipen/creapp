<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Creappus');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>   

	<?php
		echo $this->Html->meta('icon');

		
		 echo $this->Html->css('bootstrap');
  		 echo $this->Html->css('font-awesome.min');
  		  echo $this->Html->css('mdb');
  		 echo $this->Html->css('style'); 

  		  echo $this->Html->script('tether.min.js');
		 echo $this->Html->script('bootstrap.min');
		// echo $this->Html->script('mdb.min.js');
		
		 

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">		
		<div class="wrapper" >

			<?php echo $this->element('header');?>

	
			<div class="">
				<?php echo $this->fetch('content'); ?>
			</div>
			<?php echo $this->Session->flash(); ?>
		</div>
	</div>
	<script type="text/javascript">
 $(document).ready(function() 
	 	{
		 	$("#flashMessage").fadeOut(4000, function()
		     {
		      $("#flashMessage").hide();
		    });
		});        
</script>
</body>
</html>
