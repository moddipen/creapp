 
     

         <nav class="navbar navbar-dark navbar-fixed-top scrolling-navbar">
        <!-- Collapse button-->
        <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx">
            <i class="fa fa-bars"></i>
        </button>
        <div class="container">
            <!--Collapse content-->
            <div class="collapse navbar-toggleable-xs" id="collapseEx">
                <!--Navbar Brand-->
                <a class="navbar-brand" href="index.html" target="_blank"><img src="<?php echo $this->webroot;?>img/logo.svg?>" width="50px" class="logo-text">CREapp.us</a>
                <!--Links-->
                <a id="menu-toggle" href="#" class="navbar-toggler"><i class="fa fa-bars"></i></a>
                 <?php 

                    $log_user = $this->Session->read("Auth.User");
                    if(!empty($log_user)) { ?>
                    
               
                <div id="sidebar-wrapper">
                    <ul class="sidebar-nav">
                        <a id="menu-close" href="#" class="btn btn-default btn-close btn-lg pull-right toggle"><i class="fa fa-close"></i></a>
                        <li class="nav-item main-heading" style="margin-top:65px;">
                            <a class="nav-link">Main</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link sub-heading" href="<?php echo configure::read('SITE_URL');?>users/browse"><i class="fa fa-list-ul menu-font"></i> Browse</a>
                        </li><hr>
                         <li class="nav-item active">
                            <a class="nav-link sub-heading" href="<?php echo configure::read('SITE_URL');?>users/publish"><i class="fa fa-plus-circle menu-font"></i> Publish</a>
                        </li>
                        <li class="nav-item main-heading">
                            <a class="nav-link ">Account</a>
                        </li>
                         <li class="nav-item sub-heading">
                            <a class="nav-link" href="<?php echo configure::read('SITE_URL');?>users/profile"><i class="fa fa-user menu-font"></i> Profile</a>
                        </li><hr>
                         <li class="nav-item sub-heading">
                            <a href="<?php echo configure::read('SITE_URL');?>users/share" class="nav-link"><i class="fa fa-share-alt menu-font"></i> Share my List</a>
                        </li><hr>
                        <li class="nav-item sub-heading">
                            <a class="nav-link" href="<?php echo configure::read('SITE_URL');?>users/messages"><i class="fa fa-envelope-o menu-font"></i>Messages</a>
                        </li><hr>
                         <li class="nav-item sub-heading">
                            <a href="<?php echo configure::read('SITE_URL');?>users/logout" class="nav-link"><i class="fa fa-sign-out menu-font"></i>Logout</a>
                        </li>
                         <li class="nav-item main-heading">
                            <a class="nav-link"> Info</a>
                        </li>
                          <li class="nav-item sub-heading">
                            <a class="nav-link" href="<?php echo configure::read('SITE_URL');?>users/about"><i class="fa fa-info-circle menu-font"></i> About</a>
                        </li><hr>
                         <li class="nav-item sub-heading"> 
                            <a href="#" class="nav-link"><i class="fa fa-question-circle menu-font"></i> Help</a>
                        </li><hr>
                         <li class="nav-item sub-heading">
                            <a href="#" class="nav-link"><i class="fa fa-paperclip menu-font"></i> legal</a>
                        </li>
                    </ul>
                </div>

                <?php
                    }
                    else
                    {
                ?>
                <div id="sidebar-wrapper">
                    <ul class="sidebar-nav">
                        <a id="menu-close" href="#" class="btn btn-default btn-close btn-lg pull-right toggle"><i class="fa fa-close"></i></a>
                        <li class="nav-item main-heading" style="margin-top:65px;">
                            <a class="nav-link">Main</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link sub-heading" href="<?php echo configure::read('SITE_URL');?>users/browse"><i class="fa fa-list-ul menu-font"></i> Browse</a>
                        </li>
                        <li class="nav-item main-heading">
                            <a class="nav-link ">Account</a>
                        </li>
                         <li class="nav-item sub-heading">
                            <a class="nav-link" href="<?php echo configure::read('SITE_URL');?>users/login">Login</a>
                        </li><hr>
                         <li class="nav-item sub-heading">
                            <a href="<?php echo configure::read('SITE_URL');?>users/signup" class="nav-link">Register</a>
                        </li>
                         <li class="nav-item main-heading">
                            <a class="nav-link"> Info</a>
                        </li>
                          <li class="nav-item sub-heading">
                            <a class="nav-link" href="<?php echo configure::read('SITE_URL');?>users/about"><i class="fa fa-info-circle menu-font"></i> About</a>
                        </li><hr>
                         <li class="nav-item sub-heading"> 
                            <a href="#" class="nav-link"><i class="fa fa-question-circle menu-font"></i> Help</a>
                        </li><hr>
                         <li class="nav-item sub-heading">
                            <a href="#" class="nav-link"><i class="fa fa-paperclip menu-font"></i> legal</a>
                        </li>
                    </ul>
                </div>
                <?php 
            } 
            ?>
            <!--/.Collapse content-->
        </div>
    </nav>



<script type="text/javascript">
       $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
      });
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
      });
 </script>

    