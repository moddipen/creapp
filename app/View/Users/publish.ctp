<body class="">

 <section class="main-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5">
                    <div class="card left-card publish-card">
                        <div class="row">
                            <div class="col-md-6 active">
                                <a href="<?php echo configure::read('SITE_URL');?>users/publish">Publish</p>
                            </div>
                            <div class="col-md-6">
                                <a href="<?php echo configure::read('SITE_URL');?>users/browse">Close</a>
                            </div>
                        </div>
                    </div>
                    <div class="comments" id="style-1">
                        <form>
                        <select class="mdb-select">
                            <option value="" disabled selected>Any</option>
                            <option value="1">Retail</option>
                            <option value="2">Office</option>
                            <option value="3">Leisure</option>
                        </select>                        
                        <label style="top: 72px;">Type*</label>

                            <div class="md-form">
                                <!-- <i class="fa fa-user prefix"></i> -->
                                <input type="text" id="user" class="form-control" required="required">
                                <label for="user" class="">Title</label>
                            </div>
                            
                            <div class="form-group">
                                <input type="radio" class="with-gap" id="draw" name="draw">
                                <label for="radio4">Draw on map</label>
                            </div>
                             <div id="drawpoly" style="display: none">
                             	<a href="" class="btn btn-form edit-btn"><b>EDIT OR DELETE AREA</b></a>
                              	<a href="" class="btn btn-form edit-btn"><b>DRAW NEW AREA</b></a>
                             </div>
                            <div class="form-group">
                                <input type="radio" class="with-gap" id="input" name="input">
                                <label for="radio5">Input Location</label>
                            </div>
                            <div id="dvinput" style="display: none">
                            	<input type="text" id="user" class="form-control" required="required">
                                <label for="user" class="">Radius</label>
                            	
                             </div>
                           
                            <div class="md-form">
                                <textarea type="text" id="form7" class="md-textarea"></textarea>
                                <label for="form7">Textarea</label>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="checkbox1">
                                <label for="checkbox1">Classic checkbox</label>
                            </div>
                             <div class="form-group">
                                <input type="checkbox" id="checkbox2">
                                <label for="checkbox2">Classic checkbox</label>
                            </div>
                            
                            <div class="file-field">
                                <div class="btn btn-primary btn-sm">
                                    <span>Choose files</span>
                                    <input type="file" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload one or more files">
                                </div>
                            </div>
                            <div class="text-center" style="padding-top: 35px">
                                <button class="btn btn-default">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="map">
                        <iframe width="100%" height="900px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265&amp;output=embed"></iframe><br><small><a href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265" style="color:#8C8061;text-align:left;font-size:1em">View Larger Map</a></small>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script type="text/javascript">
         $(document).ready(function() {
    $('.mdb-select').material_select();
  });

    $(function () {
        $("input[name='draw']").click(function () {
            if ($("#draw").is(":checked")) {
                $("#drawpoly").show();
            } else {
                $("#drawpoly").hide();
            }
        });
        $("input[name='input']").click(function () {
            if ($("#input").is(":checked")) {
                $("#dvinput").show();
            } else {
                $("#dvinput").hide();
            }
        });
    });
    </script>
</body>