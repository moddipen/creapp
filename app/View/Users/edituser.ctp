  <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Update User</h3>
                 <center> <h5><?php echo $this->Session->flash();?></h5></center>
              </div>

                <div class="title_right">
                  <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                  </div>
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content">
                    <br />
                 
        <?php echo $this->Form->create('User',array('novalidate' => true,'class'=>'form-horizontal form-label-left','id'=>'edituser'));?>
                  
                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span> </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                           <?php echo $this->Form->input('User.name',array('label'=>false,'class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Enter first name','value'=>$user['User']['name'])); ?>
                      </div>
                  </div>

                  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php 
                    echo $this->Form->input('User.email',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter Email','value'=>$user['User']['email']));
                    ?>
                        </div>
                  </div>
                      
                  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div id="gender" class="btn-group" >
                            <label class="" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <?php 
                    $options=array('0'=>'Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', '1'=>'Female');
                     $attributes=array('legend'=>false,'label'=>false,'class'=>'radio radio-inline active-radio','value'=>$user['User']['gender']);
                        echo $this->Form->radio('User.gender',$options,$attributes);
                    ?>
                            </label>
                          </div>
                        </div>
                  </div>

                 
                  <div class="form-group">
                        <label for="location" class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <?php 
                    echo $this->Form->input('User.location',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter Location','value'=>$user['User']['location']));
                    ?>
                        </div>
                  </div>
                      
                       
                      <?php
                      echo $this->Form->input('User.usertype',array('type'=>'hidden','value'=>$user['User']['usertype']));
                      echo $this->Form->input('User.id',array('type'=>'hidden','value'=>$user['User']['id']));
                    ?>
                 
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                       <?php echo $this->Form->end();?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
