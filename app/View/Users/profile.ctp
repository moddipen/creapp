 <body class="">
    <!-- Content Header (Page header) -->
    <section class="header-section-login text-xs-center">
        <h1><b>WELCOME</b></h1>
         <h4></h4>
    </section>

    <!-- Main content -->
      <section class="login-backimg">
        <div class="container">
            <div class="row">
                <div class="col-md-7 offset-md-2 login-card">
                    <div class="login-form">
                        <div class="register nav-tabs">
                            <h2>PROFILE</h2>  
                        </div>
                        <div class="account-login">
                            <div class="card-block">
                            <?php 
                             //debug($user); exit;    
                              if($user_data['User']['log_linked'] == 1){    
                              ?>

                              <h3 class="profile-username text-center"><?php echo $user_data['User']['first_name'].' '.$user_data['User']['last_name'];?></h3>
                              <div class="profile-box">
                               <label class="profile-username text-center">RE License #:<?php echo $user_data['User']['license'];?></label>

                               <?php  if(!empty($user_data['User']['profile'])) { ?>

                                <img src="<?php echo $user_data['User']['profile'];?>" class="img-circle" alt="User Image" style="width:150px;height:150px;">
                               <?php } 
                               else
                                { ?>
                               <img src="<?php echo $this->webroot; ?>img/user.png" class="img-circle" alt="User Image" style="width:150px;height:150px;">  
                                <?php } ?>
                                </div> 
                              <?php }
                              else
                                { ?>

                               <h3 class="profile-username text-center"><?php echo $user_data['User']['first_name'].' '.$user_data['User']['last_name'];?></h3>
                               <div class="profile-box">
                                <label class="profile-username text-center">RE License #:<?php echo $user_data['User']['license'];?></label>

                                 <?php  if(!empty($user_data['User']['profile'])) { ?>
                              <img src="<?php echo $this->webroot; ?>img/uploads/small/<?php echo $user_data['User']['profile'];?>" class="img-circle" alt="User Image" style="width:150px;height:150px;">
                               <?php } else { ?>
                               <img src="<?php echo $this->webroot; ?>img/user.png" class="img-circle" alt="User Image" style="width:150px;height:150px;">          
                                <?php } ?>
                              </div>  
            <?php }
             ?>
                     <center>
                     <div>              
                       <a href="<?php echo Configure::read('SITE_URL')?>users/editprofile" class="btn btn-form"><b>EDIT PROFILE</b></a>
                     </div>
                    </center>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 </section>
 <style type="text/css">
   .profile-box{
    height: 180px;
   }
   .profile-box img{
      float: right;
   }
 </style>
</body>