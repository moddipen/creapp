<!DOCTYPE html>
<html>
<head>
 
</head>
<body class="">

<section class="header-section-login text-xs-center">
        <h1><b>WELCOME</b></h1>
        <h4>Please Regiser to Access Your Account!!</h4>
    </section>

    <section class="login-backimg">
        <div class="container">
            <div class="row">
                <div class="col-md-7 offset-md-2 login-card">
                    <div class="login-form">
                        <div class="register nav-tabs">
                            <h2>REGISTER</h2>  
                        </div>
                        <div class="account-login">
                            <div class="card-block">
                                <h3 class="doc-title-content">Create Account</h3>
                                 <?php echo $this->Form->create('User',array('acion'=>'signup',"id"=>"edit-profile",'novalidate' => true));?>
                                    <div class="md-form">
                                        <i class="fa fa-user prefix"></i>
                                      
                                        <?php 
                   echo $this->Form->input('User.first_name',array('type'=>'text','id'=>'sponser','label'=>false,'div'=>false,'class'=>"form-control"));
                   ?>
                                        <label for="sponser" class="">First Name</label>
                                    </div>
                                    <div class="md-form">
                                        <i class="fa fa-user prefix"></i>
                                        
                                         <?php 
                   echo $this->Form->input('User.last_name',array('type'=>'text','label'=>false,'div'=>false,'class'=>"form-control",'id'=>'username'));
                   ?>
                                        <label for="username" class="">LastName</label>
                                    </div>
                                    <div class="md-form">
                                        <i class="fa fa-envelope prefix"></i>
                                        
                                         <?php 
                   echo $this->Form->input('User.email',array('type'=>'text','label'=>false,'div'=>false,'class'=>"form-control",'id'=>'email'));
                   ?>
                   
                                        <label for="email">Email</label>
                                    </div>
                                    <div class="md-form">
                                        <i class="fa fa-lock prefix"></i>
                                        <?php 
                   echo $this->Form->input('User.password',array('label'=>false,'div'=>false,'class'=>"form-control",'id'=>'password'));
                   ?>
                                        <label for="password">Password</label>
                                    </div>
                                    <div class="md-form">
                                        <i class="fa fa-credit-card prefix"></i>
                                         <?php 
                   echo $this->Form->input('User.license',array('type'=>'text','label'=>false,'div'=>false,'class'=>"form-control",'id'=>'fullname'));
                   ?>
                                        <label for="fullname">RE license#</label>
                                    </div>
                                    <fieldset class="form-group">
                                        <input type="checkbox" id="agree" required="required">
                                        <label for="agree">I Agree to the <b>Terms and Condition</b></label>
                                    </fieldset>
                                    <div>
                                        <button type="submit" class="btn btn-form" style="width:20px , height:20px">REGISTER</button>
                                    </div>
                                   <?php echo $this->Form->end();?>
                                   <div>
                                     <a href="<?php echo configure::read('SITE_URL');?>users/login" class="text-center">BACK TO LOGIN</a>
                                     </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 </section>
<!-- /.register-box -->

<script type="text/javascript">
 
    
 $(document).ready(function() {
    $('select').material_select();
  }); 
</script>

</body>
</html>
