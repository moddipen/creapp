<body class="">
<section class="header-section-login text-xs-center">
        <h1><b>WELCOME</b></h1>
         <h4></h4>
       
  </section>


  <section class="login-backimg">
        <div class="container">
            <div class="row">
                <div class="col-md-7 offset-md-2 login-card">
                    <div class="login-form">
                        <div class="register nav-tabs">
                            <h2>ABOUT US</h2>  
                        </div>
                         </div>
                </div></div>
                <section class="about-section">
                            <p>CREapp (Commercial Real Estate Application) is a free of charge platform which allows:</p>
            <ul style="margin-left: 40px">
                <li>Registered commercial real estate brokers to directly publish purchase and lease requirements.</li>
                <li>To connect purchasers and lessees with registered commercial real estate brokers who can qualify and publish their requirements.</li>
                <li>Sellers and lessors of commercial real estate to efficiently search for purchase and lease requirements.</li>
            </ul>
            <p>This platform is primarily meant to be used by commercial real estate brokers as an advertising tool to consolidate and publish qualified purchase/lease requirements.</p>
            <p>We will never charge for the above listed services and strive to make it as efficient as possible. We believe the free flow and accessibility of such information are beneficial to the industry and all parties involved. We will not be involved or remunerated for any relationship or transaction arising out of the use of this service.</p>
                   </section>
            
        </div>
    </section>
<!-- <section class="about-section"> 
       <div class="container" style="margin-top:100px;">
            <h2>About Us</h2><span></span>
            <p>CREapp (Commercial Real Estate Application) is a free of charge platform which allows:</p>
            <ul style="margin-left: 40px">
                <li>Registered commercial real estate brokers to directly publish purchase and lease requirements.</li>
                <li>To connect purchasers and lessees with registered commercial real estate brokers who can qualify and publish their requirements.</li>
                <li>Sellers and lessors of commercial real estate to efficiently search for purchase and lease requirements.</li>
            </ul>
            <p>This platform is primarily meant to be used by commercial real estate brokers as an advertising tool to consolidate and publish qualified purchase/lease requirements.</p>
            <p>We will never charge for the above listed services and strive to make it as efficient as possible. We believe the free flow and accessibility of such information are beneficial to the industry and all parties involved. We will not be involved or remunerated for any relationship or transaction arising out of the use of this service.</p>
       </div>
    </section> -->
    </body>