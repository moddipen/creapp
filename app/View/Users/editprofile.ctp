<body class="">
   
     <section class="header-section-login text-xs-center">
        <h1><b>WELCOME</b></h1>
         <h4></h4>
       
  </section>

 <section class="login-backimg">
        <div class="container">
            <div class="row">
                <div class="col-md-7 offset-md-2 login-card">
                    <div class="login-form">
                        <div class="register nav-tabs">
                            <h2>EDIT PROFILE</h2>  
                        </div>
                        <div class="account-login">
                            <div class="card-block">
                               <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <?php echo $this->Form->create('User',array('novalidate' => true,'class'=>'form-horizontal','id'=>'editprofile','enctype'=>'multipart/form-data'));?>
          <div class="box box-primary">
                   
           <?php 
            $user = $this->Session->read('Auth.User');


           // debug($user); exit;    
            if($user['log_linked'] == 1) {  ?>
              <div class="box-body">

              <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">First name </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.first_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter first name','value'=>$user['first_name']));
                      ?>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Last name </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.last_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter last name','value'=>$user['last_name']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">License </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.license',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Enter license','value'=>$user['license']));
                      ?>
                    </div>
                  </div>
                
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Email </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.email',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter Email','value'=>$user['email']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Telephone </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.phone',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Enter phone number','value'=>$user['phone']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Title </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.title',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter title','value'=>$user['title']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Company Name </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.company_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter company name','value'=>$user['company_name']));
                      ?>
                    </div>
                  </div>
                   <div class="file-field">
                    <div class="btn btn-primary btn-sm upload-btn">
                        <span>Choose files</span>
                        <?php 
                       echo $this->Form->file("User.img",array('doc_file',"label"=>false,'class'=>'pro-img','type' => 'file','id'=>'imgInp'));
                              
                       echo $this->Form->input("User.profile",array("type"=>"hidden","value"=>""));
                      ?>
                    </div>
                    <div class="file-path-wrapper" style="width: 90%">
                        <input class="file-path validate" type="text" placeholder="Upload Profile">
                    </div>
                   <img src="<?php echo $user['profile'];?>" id="img"  alt="Profile Image" style="width:80px;height:80px;margin-left: 76%;"/>  
                </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Description </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.description',array('label'=>false,'class'=>'form-control','placeholder'=>'','type'=>'textarea'));                       
                      
                      ?>
                    </div>
                  </div>      
                    <?php
                      echo $this->Form->input('User.usertype',array('type'=>'hidden','value'=>$user['usertype']));
                      echo $this->Form->input('User.id',array('type'=>'hidden','value'=>$user['id']));
                    ?>                  
              </div>

              <?php } else { ?>

                 <div class="box-body">
                   <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">First name </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.first_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter first name','value'=>$user['first_name']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Last name </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.last_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter last name','value'=>$user['last_name']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">License </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.license',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Enter license','value'=>$user['license']));
                      ?>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Email </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.email',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter Email','value'=>$user['email']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Telephone </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.phone',array('type'=>'text','label'=>false,'class'=>'form-control','placeholder'=>'Enter phone number','value'=>$user_data['User']['phone']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Title </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.title',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter title','value'=>$user_data['User']['title']));
                      ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Company Name </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.company_name',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter company name','value'=>$user_data['User']['company_name']));
                      ?>
                    </div>
                  </div>


                  <div class="file-field">
                    <div class="btn btn-primary btn-sm ">
                        <span>Choose files</span>
                        <?php 
                       echo $this->Form->file("User.img",array('doc_file',"label"=>false,'class'=>'pro-img','type' => 'file','id'=>'imgInp'));
                              
                       echo $this->Form->input("User.profile",array("type"=>"hidden","value"=>""));
                      ?>
                    </div>
                    <div class="file-path-wrapper" style="width: 90%">
                        <input class="file-path validate" type="text" placeholder="Upload Profile">
                    </div>
                   <img src="<?php echo $this->webroot;?>img/uploads/small/<?php echo $user['profile'];?>" id="img"  alt="Profile Image" style="width:80px;height:80px;margin-left: 76%;"/>  
                </div>

                   <!-- <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Picture </label>

                    <div class="col-sm-9">
                      <?php 
                       echo $this->Form->file("User.img",array('doc_file',"label"=>false,'class'=>'pro-img','type' => 'file','id'=>'imgInp'));
                              
                       echo $this->Form->input("User.profile",array("type"=>"hidden","value"=>""));
                      ?>
                    </div>
                    <img src="<?php echo $this->webroot;?>img/uploads/small/<?php echo $user['profile'];?>" id="img"  alt="Profile Image" style="width:80px;height:80px;margin-left: -90px;"/> 
                  </div> -->
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Description </label>

                    <div class="col-sm-9">
                      <?php 
                      echo $this->Form->input('User.description',array('label'=>false,'class'=>'form-control','placeholder'=>'','type'=>'textarea','value'=>$user_data['User']['description']));                           
                       
                      ?>
                    </div>
                  </div>  
                      <?php
                        echo $this->Form->input('User.usertype',array('type'=>'hidden','value'=>$user_data['User']['usertype']));
                        echo $this->Form->input('User.id',array('type'=>'hidden','value'=>$user_data['User']['id']));
                      ?>
                </div>
              <?php } ?>
              
              <div class="box-footer">
                  <div class="box-footer" align="center">  
                    <div class="row">
                     <div class="col-md-4">
                        <button type="submit" id="formsubmit" class="btn btn-form" style="font-weight: 700">SAVE CHANGES</button>
                   <!--  </div>   --> 
                    </div>         
                         <?php echo $this->Form->end();?>
                    <div class="col-md-4">
                      <a href="<?php echo Configure::read('SITE_URL')?>users/profile" class="btn btn-form edit-btn"><b>VIEW PROFILE</b></a>
                    </div>
                    <div class="col-md-4">
                      <a href="<?php echo Configure::read('SITE_URL')?>users/profile" class="btn btn-form edit-btn"><b>CANCEL</b></a>
                    </div>
                   </div>
                   <center>
                      <a href="<?php echo Configure::read('SITE_URL')?>users/delete" class="btn btn-form edit-btn" style="
    margin-top: 24px;"><b>DELETE ACCOUNT</b></a>
                    </center>
                 </div>          
        </div>       
      </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 </section>
 <script type="text/javascript">
     function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();            
              reader.onload = function (e) {
                  $('#img').attr('src', e.target.result);
              }            
              reader.readAsDataURL(input.files[0]);
          }
      }    
      $("#imgInp").change(function(){
          readURL(this);
      });
    $(document).ready(function() {
            $('select').material_select();
            
          }); 
    document.getElementById("uploadBtn").onchange = function () {
    document.getElementById("uploadFile").value = this.files[0].name;
 </script>

</body>
  <style type="text/css">
      .pro-img{
          margin-top: 7px;
          }
          .control-label{
            padding-top: 16px;
          }
          .file-field input[type=file]{
            position: initial !important;
          }
          .file-field .file-path-wrapper{
            width: 71% !important;
          }
          .file-field .btn{
            float: left;
    line-height: 1rem;
    padding-top: 12px;
    height: 40px;
          }
          .file-field{
            position: initial !important;
          }
          .file-field input.file-path{
            width: 92% !important;
          }
  </style>













  




