<?php
	class User extends AppModel
	{
		var $name='User';
         
        //public $recursive = -1;
       
		//validation of all fields...
        
		public $validate = array(
         
           'email'=>array(
                'Valid email'=>array(
                'rule'=>array('email'),
                'message'=>'Please enter a valid email address'
            ),
                'uniqueEmailRule' => array(
                'rule' => 'isUnique',
                'message' => 'Email already registered'
            ),        
           
        ),

            'password'=>array(
                'length' => array(
                    'rule' => array('notBlank'),
                'message' => 'You must enter a password.'
                
                ),
            ),
            'confirm_password' => array(
                'length' => array(
              'rule' => array('notBlank'),
                'message' => 'You must enter a confirm password.'
            ),
                'compare'    => array(
                'rule'      => array('validate_passwords'),
                'message'   => 'The passwords you entered do not match.',
                'allowEmpty' => false
                 ),
                ),
                 
            
	       );
	       public function equaltofield($check,$otherfield) 
           { 
                 //get name of field 
                 $fname = ''; 
                foreach ($check as $key => $value)
                {       
                    $fname = $key; 
                    break; 
                } 
                return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname]; 
            }

            public function validate_passwords() 
    	    {
             	return $this->data[$this->alias]['password'] === $this->data[$this->alias]['confirm_password'];
    	    }

    		public function beforeSave($options = array()) 
            {
    		    // hash our password

    		    if (isset($this->data[$this->alias]['password'])) 
                {
    			     $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
    		    }

		        // if we get a new password, hash it
		        if (isset($this->data[$this->alias]['password_update'])) 
                {
			         $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
		        }
	             //$this->data['User']['resetkey'] = Security::hash(mt_rand(),'md5',true);
		        // fallback to our parent
		        return parent::beforeSave($options);
	        }

           
	}
?>