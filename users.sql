-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 12, 2017 at 10:48 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Creap`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `license` varchar(255) NOT NULL,
  `usertype` int(11) NOT NULL COMMENT '0 = buyer, 1 = broker, 2 = seller',
  `phone` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `profile` varchar(255) NOT NULL,
  `log_linked` int(11) NOT NULL COMMENT '0 = default, 1 = login_linked',
  `status` int(11) NOT NULL COMMENT '0 = Inactive, 1 = Active ',
  `token` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `license`, `usertype`, `phone`, `title`, `company_name`, `profile`, `log_linked`, `status`, `token`, `description`, `created`, `modified`) VALUES
(13, 'Bharat', 'Kokcha', 'dimple@teknomines.com', 'd1afe819f09c4ca49eef36f553f5411e', 'test', 0, '', '', '', '', 0, 0, '', '', '2017-08-05 15:02:21', '2017-08-05 15:02:21'),
(14, 'BIrva', 'Rajgor', 'birva@gmail.com', 'd1afe819f09c4ca49eef36f553f5411e', 'demo', 0, '', '', '', '', 0, 0, '', '', '2017-08-09 14:50:44', '2017-08-09 14:50:44'),
(15, 'dipen', 'modi', 'teknomins9@gmail.com', '', '', 0, '', '', '', 'https://media.licdn.com/mpr/mprx/0_02S6r545wi_aEyJy1gzQU6tkfNn1Wf4Tv2R6nl55IiFtW0IDrgBHj-O5HiP7ombDyHBHZlALGLN-7IkjKxeV4lrdbLNP7I7hKxeL1KLXuXT05jNypaOwPXIzFKm_6I4AJp2oNClfXmF', 1, 0, '', '', '2017-08-12 08:26:52', '2017-08-12 08:26:52'),
(16, 'Dimple', 'Munjani', 'testdimple0@gmail.com', 'd1afe819f09c4ca49eef36f553f5411e', 'demo', 0, '', '', '', '1501844839.jpeg', 0, 0, '', '', '2017-08-12 08:28:58', '2017-08-12 08:28:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
